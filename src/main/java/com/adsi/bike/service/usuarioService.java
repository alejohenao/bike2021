package com.adsi.bike.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.s
import org



        @Service
public class UsuarioService implements UserDetailsService {

    private Logger logger= LoggerFactory.getLogger(UsuarioService.class);

    @Autowired
            private UserRepository userRepository;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUsername(String username) throws UsernameNotFoundException {
        Users usuario = userRepository.findByUsername(username);

        if(usuario == null){
            logger.error("Error en el login:  no existe el usuario'" + " ' ");
            throw new UsernameNotFounException("Error en el login:  no existe el usuario'" + username + "")
        }

        List<GrantedAuthority> authorities= usuario.getRols()
                .stream()
                .map(role -> new SimpleGrantedAuthority(role.getDescription()))
                .peek(authority ->logger.info("Role:  " + authority.getAuthority()))
                .collec(Collectors.toList());

                return null User(usuario.getUsername(), usuario.getPassword(), usuario.getEnabled(), accountNonExpire);
    }
        }
