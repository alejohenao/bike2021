package com.adsi.bike.service.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SaleFilterDTO {

    private int idSale;
    private String model;
    private String nameClient;
    private String email;
    private Double price;
}
