package com.adsi.bike.service.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class BikeDTO {

    @NotNull
    private int id;

    @NotEmpty(message = "Este campo no puede estar vacio")
    @Size(min = 2, max =10, message ="El tamaño del campo debe estar entre dos y diez caracteres")
    private String model;
    @NotEmpty(message = "Este campo no puede estar vacio")
    @Size(min = 2, max =10, message ="El tamaño del campo debe estar entre dos y diez caracteres")
    private String serial;
    private double price;

}
