package com.adsi.bike.service.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DataUserDTO {

    private String userName;
    private String firstName;
    private String lastName;
    private String documentNumber;

}
