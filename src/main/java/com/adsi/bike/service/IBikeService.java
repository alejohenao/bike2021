package com.adsi.bike.service;

import com.adsi.bike.domain.Bike;
import com.adsi.bike.service.dto.BikeDTO;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface IBikeService {

    // get - obtener - read
    public Page<BikeDTO> read(Integer pageSize, Integer pageNumber);

    // put - actualizar - update
    public Bike update(Bike bike);

    // Delete - borrar - delete
    public void delete(Integer id);

    // post - guardar - create
    public ResponseEntity create (BikeDTO bikeDTO);

public Optional<Bike> getById(Integer id);

public ResponseEntity search(String serial, String model);

public Integer quantityBikes();
//Inventory value
public Double inventoryValue();

//find price and model by model
    public Iterable<Bike> findByModelQuery(String model);
}
