package com.adsi.bike.service;

import com.adsi.bike.domain.Client;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface IClientService {

    public Iterable<Client> read();

    public Optional<Client> getById(Integer id);

    public ResponseEntity create(Client client);

    public Client update(Client client);

    public void delete(Integer id);

    public ResponseEntity search (String name, String documentNumber, String email);

    public Iterable<Client> findClientByDocumentContains (String document);
}
