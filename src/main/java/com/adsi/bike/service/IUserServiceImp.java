package com.adsi.bike.service;


import com.adsi.bike.domain.DetailUser;
import com.adsi.bike.domain.Users;
import com.adsi.bike.repository.DetailUserRepository;
import com.adsi.bike.repository.UserRepository;
import com.adsi.bike.service.dto.DataUserDTO;
import com.adsi.bike.service.transformer.DataUserTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class IUserServiceImp implements IUserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    DetailUserRepository detailUserRepository;

    @Override
    public Iterable<Users> read() {
        return userRepository.findAll();
    }

    @Override
    public Optional<Users> getById(Long id) {
        return userRepository.findById(id);
    }

    @Override
    public ResponseEntity create(Users users) {
        return new ResponseEntity(userRepository.save(users), HttpStatus.OK);
    }

    @Override
    public Users update(Users users) {
        return userRepository.save(users);
    }

    @Override
    public DataUserDTO getDataUser(String documentNumber) {
        DetailUser detailUser = detailUserRepository.findByDocumentNumber(documentNumber);
        Users users = userRepository.findByDetailUser(detailUser);
        DataUserDTO dataUserDTO = DataUserTransformer.getDataUserDTO(detailUser, users);
        return dataUserDTO;
    }
}
