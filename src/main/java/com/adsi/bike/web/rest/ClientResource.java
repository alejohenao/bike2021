package com.adsi.bike.web.rest;

import com.adsi.bike.domain.Client;
import com.adsi.bike.service.IClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api")
public class ClientResource {
    @Autowired
    IClientService clientService;

    @PostMapping("/client")
    public ResponseEntity create(@RequestBody Client client){
        return clientService.create(client);
    }
    @GetMapping("/client")
    public Iterable<Client> read(){
        return clientService.read();
    }
    @GetMapping("/client/{id}")
    public Optional<Client> getById(@PathVariable Integer id){
        return clientService.getById(id);
    }
    @PutMapping("/client")
    public Client update(@RequestBody Client client){
        return clientService.update(client);

    }

    //client/search?model=B124
    @GetMapping("client/search")
    public ResponseEntity search(@RequestParam(value = "name", required = false) String name,
                                 @RequestParam(value = "documentNumber",required = false) String documentNumber,
                                 @RequestParam(value = "email", required = false) String email){
        return clientService.search(name,documentNumber,email);
    }

    @GetMapping("/client/find-by-document")
    public Iterable <Client> findClientByDocument (@RequestParam(value = "document.contains", required = false) String document){
        return clientService.findClientByDocumentContains(document);
    }

}
